import com.signavio.exception.WorkflowMalformedException;
import com.signavio.model.Status;
import com.signavio.model.Workflow;
import com.signavio.model.WorkflowInstance;
import com.signavio.validation.EntityValidator;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import uk.org.lidalia.slf4jtest.TestLogger;
import uk.org.lidalia.slf4jtest.TestLoggerFactory;

import java.net.URISyntaxException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.CoreMatchers.*;

public class ValidationTest {
    private Path directoryPath;

    private static final TestLogger logger = TestLoggerFactory.getTestLogger(EntityValidator.class);

    @Before
    public void setup() throws URISyntaxException {
        directoryPath = Paths.get("src","test","resources","correct");

    }

    @After
    public void clearLoggers() {
        TestLoggerFactory.clear();
    }

    @Test
    public void givenInconsistencyInWorkflowInstanceId_ValidationreturnsFalse() throws Exception {
        EntityValidator entityValidator = new EntityValidator(directoryPath);
        WorkflowInstance workflowInstance = new WorkflowInstance
                (3683228499925040330L,
                        4711L,
                        "f.consultant@example.org",
                        Status.NEW,
                        "supervisor approval");
        entityValidator.validate(workflowInstance);
        assertThat(logger.getLoggingEvents().get(0).getArguments(), is(Arrays.asList(workflowInstance,workflowInstance.getWorkflowId())));
    }

    @Test
    public void givenInconsistencyInWorkflow_ValidationreturnsFalse() throws WorkflowMalformedException {
        EntityValidator entityValidator = new EntityValidator(directoryPath);
        Workflow workflow = new Workflow(1L,"Purchase Request Approval Sub-Workflow",
                "nonexist@company.local",14);
        entityValidator.validate(workflow);
        assertThat(logger.getLoggingEvents().get(0).getArguments(), is(Arrays.asList(workflow,workflow.getAuthor())));
    }

    @Test
    public void givenInconsistencyInWorkflowInstanceAssigne_ValidationreturnsFalse() throws Exception {
        EntityValidator entityValidator = new EntityValidator(directoryPath);
        WorkflowInstance workflowInstance = new WorkflowInstance
                (3683228499925040330L,
                        1L,
                        "not_existed@example.org",
                        Status.NEW,
                        "supervisor approval");
        entityValidator.validate(workflowInstance);
        assertThat(logger.getLoggingEvents().get(0).getArguments(), is(Arrays.asList(workflowInstance,workflowInstance.getAssignee())));
    }

}
