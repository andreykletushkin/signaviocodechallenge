import com.signavio.model.Workflow;
import com.signavio.model.WorkflowInstance;
import com.signavio.service.DataAnalyzer;
import com.signavio.service.DataLoader;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import uk.org.lidalia.slf4jtest.TestLogger;
import uk.org.lidalia.slf4jtest.TestLoggerFactory;

import java.net.URISyntaxException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;


import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.CoreMatchers.*;

public class DataAnalyzerTest {

    private Path directoryPath;
    private Path malformed;

    private final static TestLogger logger = TestLoggerFactory.getTestLogger(DataAnalyzer.class);
    private DataLoader dataLoader;
    private DataAnalyzer dataAnalyzer;
    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Before
    public void setup() throws URISyntaxException {
        directoryPath = Paths.get("src","test","resources","correct");
        malformed = Paths.get("src","test","resources","malformed");
        dataLoader = new DataLoader(directoryPath);
        dataAnalyzer = new DataAnalyzer(directoryPath);
    }

    @After
    public void clearLoggers() {
        TestLoggerFactory.clear();
    }

    @Test
    public void whenExecutesAnalyzeEntity_WorkflowWithInstanceListReported() {
        dataAnalyzer.generateInstanceReport();
        List<WorkflowInstance> workflowInstanceList = dataLoader.getInstancesById(1L);
        assertThat(logger.getLoggingEvents().get(1).getArguments().get(1),is(workflowInstanceList));
    }

    @Test
    public void whenExecutesGenerateRunningInstancesReport_ReportGenerated() {
        Workflow workflow = new Workflow(1L,"Purchase Request Approval Sub-Workflow",
                "john.doe@company.local",14);
        dataAnalyzer.generateRunningInstancesReport();
        assertThat(logger.getLoggingEvents().get(0).getArguments(),is(Arrays.asList(workflow,3)));
        assertThat(logger.getLoggingEvents().get(1).getArguments(),is(Arrays.asList("con99")));
    }

    @Test
    public void whenRunningInstanceReport_WithMaleformedData_ReportIsMissing() {
        DataAnalyzer dataAnalyzer = new DataAnalyzer(malformed);
        dataAnalyzer.generateRunningInstancesReport();
        assertThat(logger.getLoggingEvents().size(),is(0));
    }

}
