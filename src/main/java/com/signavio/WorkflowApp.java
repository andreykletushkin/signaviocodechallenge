package com.signavio;


import com.signavio.service.DataAnalyzer;

import java.nio.file.Path;
import java.nio.file.Paths;

public class WorkflowApp {

    public static void main(String[] args) {
        Path directoryPath = Paths.get("data");
        if (args.length>0) {
            directoryPath = Paths.get(args[0]);
        }

        DataAnalyzer dataAnalyzer = new DataAnalyzer(directoryPath);
        dataAnalyzer.validateConsistency();
        dataAnalyzer.generateInstanceReport();
        dataAnalyzer.generateRunningInstancesReport();

    }
}
