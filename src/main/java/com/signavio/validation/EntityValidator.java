package com.signavio.validation;

import com.signavio.exception.InstanceMalformedException;
import com.signavio.exception.WorkflowMalformedException;
import com.signavio.model.Workflow;
import com.signavio.model.WorkflowInstance;
import com.signavio.service.DataLoader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.file.Path;
import java.util.List;

public class EntityValidator {

    private final DataLoader dataLoader;
    private static final Logger logger = LoggerFactory.getLogger(EntityValidator.class);

    public EntityValidator(Path path) {
        this.dataLoader = new DataLoader(path);
    }

    public void validate(WorkflowInstance workflowInstance) throws InstanceMalformedException {
        if (!validateWorkflowInstance(workflowInstance)) {
                throw new InstanceMalformedException("Malformed instance"+workflowInstance);
        }
        if (!dataLoader.isPersonExists(workflowInstance.getAssignee()))
            logger.info("Inconsistency in {} assignee with:{}",workflowInstance,workflowInstance.getAssignee());
        if (!dataLoader.isWorkflowExists(workflowInstance.getWorkflowId())) {
            logger.info("Inconsistency in {}, workflow with {}",workflowInstance,workflowInstance.getWorkflowId());
        }

    }

    public void validate(Workflow workflow) throws WorkflowMalformedException {
        if (!validateWorkflow(workflow))
            throw new WorkflowMalformedException("Malformed instance"+workflow);
        if (!dataLoader.isPersonExists(workflow.getAuthor())) {
            logger.info("Inconsistency in {}, author with {}",workflow,workflow.getAuthor());
        }
    }

    public void validate(List<WorkflowInstance> workflowInstanceList) throws InstanceMalformedException {
        for(WorkflowInstance workflowInstance:workflowInstanceList)
            if (!validateWorkflowInstance(workflowInstance))
                throw new InstanceMalformedException("Malformed instance"+workflowInstance);
    }

    private boolean validateWorkflowInstance(WorkflowInstance workflowInstance) {
        if (workflowInstance.getWorkflowId() == null)
            return false;
        if (workflowInstance.getAssignee() == null)
            return false;
        if (workflowInstance.getStatus() == null)
            return false;
        if (workflowInstance.getId() == null)
            return false;
        return true;
    }

    private boolean validateWorkflow(Workflow workflow) {
        if (workflow.getId()==null)
            return false;
        if (workflow.getAuthor()==null)
            return false;
        if (workflow.getName()==null)
            return false;
        if (workflow.getVersion()==null)
            return false;
    return true;
    }



}
