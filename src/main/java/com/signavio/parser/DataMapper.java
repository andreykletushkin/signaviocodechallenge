package com.signavio.parser;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.signavio.model.*;
import com.signavio.utils.Convertors;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DataMapper {
    private ObjectMapper objectMapper = new ObjectMapper();

    public  <T> T readEntity(List<String> data,Class<T> classname) {

        Map<String,String> map = Convertors.convertListToMap(data);
        data.clear();
        return classname.cast(objectMapper.convertValue(map,classname));
    }
}
