package com.signavio.model;

public class WorkflowInstance {

    private Long id;
    private Long workflowId;
    private String assignee;
    private Status status;
    private String step;

    public WorkflowInstance(){}

    public WorkflowInstance(Long id, Long workflowId, String assignee, Status status, String step) {
        this.id = id;
        this.workflowId = workflowId;
        this.assignee = assignee;
        this.status = status;
        this.step = step;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getWorkflowId() {
        return workflowId;
    }

    public void setWorkflowId(Long workflowId) {
        this.workflowId = workflowId;
    }

    public String getAssignee() {
        return assignee;
    }

    public void setAssignee(String assignee) {
        this.assignee = assignee;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public String getStep() {
        return step;
    }

    public void setStep(String step) {
        this.step = step;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        WorkflowInstance that = (WorkflowInstance) o;

        if (!id.equals(that.id)) return false;
        if (!workflowId.equals(that.workflowId)) return false;
        if (!assignee.equals(that.assignee)) return false;
        if (status != that.status) return false;
        return step.equals(that.step);
    }

    @Override
    public String toString() {
        return "\n{" +
                "id:" + id +
                ", workflowId:" + workflowId +
                ", assignee:'" + assignee + '\'' +
                ", status:" + status +
                ", step:'" + step + '\''
                +'}';
    }
}
