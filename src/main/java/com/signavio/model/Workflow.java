package com.signavio.model;

public class Workflow {

    private Long id;
    private String name;
    private String author;
    private Integer version;

    public Workflow() {
    }

    public Workflow(Long id, String name, String author, Integer version) {
        this.id = id;
        this.name = name;
        this.author = author;
        this.version = version;
    }

    public Long getId() {
        return id;
    }

    public Workflow setId(Long id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public Workflow setName(String name) {
        this.name = name;
        return this;
    }

    public String getAuthor() {
        return author;
    }

    public Workflow setAuthor(String author) {
        this.author = author;
        return this;
    }

    public Integer getVersion() {
        return version;
    }

    public Workflow setVersion(Integer version) {
        this.version = version;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Workflow workflow = (Workflow) o;

        if (!id.equals(workflow.id)) return false;
        if (!name.equals(workflow.name)) return false;
        if (!author.equals(workflow.author)) return false;
        return version.equals(workflow.version);
    }

    @Override
    public String toString() {
        return "\n" +
                "{ id:" + id +
                ", name:'" + name + '\'' +
                ", author:'" + author + '\'' +
                ", version:" + version
                + '}';
    }
}
