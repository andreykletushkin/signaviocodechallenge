package com.signavio.model;

public enum Status {
    NEW, RUNNING, DONE, PAUSED
}
