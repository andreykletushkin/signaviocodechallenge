package com.signavio.exception;

public class InstanceMalformedException extends Exception {

    public InstanceMalformedException(String msg) {
        super(msg);
    }
}
