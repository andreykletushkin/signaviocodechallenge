package com.signavio.exception;

public class WorkflowMalformedException extends Exception {
    public WorkflowMalformedException(String msg) {
        super(msg);
    }
}
