package com.signavio.utils;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Convertors {

    public static Map<String, String> convertListToMap(List<String> list) {
        return list.stream()
                .filter(item->item.split(":").length==2)
                .filter(item->!item.split(":")[0].trim().isEmpty())
                .collect(Collectors.toMap(entry->entry.split(":")[0].trim(),entry->entry.split(":")[1].trim()));
    }
}
