package com.signavio.utils;

import com.signavio.model.Person;
import com.signavio.model.WorkflowInstance;

public class Matches {

    public static boolean contractorMatch(Long value, WorkflowInstance workflowInstance) {
            return workflowInstance.getWorkflowId().equals(value);
        }

    public static boolean contractorMatch(String email, Person person) {
        return String.valueOf(person.getEmail()).equals(email) && person.getContractorName()!=null;
    }

}
