package com.signavio.service;

import com.signavio.exception.InstanceMalformedException;
import com.signavio.exception.WorkflowMalformedException;
import com.signavio.model.Person;
import com.signavio.model.Status;
import com.signavio.model.Workflow;
import com.signavio.model.WorkflowInstance;
import com.signavio.parser.DataMapper;
import com.signavio.validation.EntityValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.file.Path;
import java.util.LinkedList;
import java.util.List;

import java.util.stream.Collectors;

public class DataAnalyzer {

    private static final Logger logger = LoggerFactory.getLogger(DataAnalyzer.class);

    private final DataLoader dataLoader;
    private final DataMapper dataMapper = new DataMapper();
    private final EntityValidator entityValidator;

    public DataAnalyzer(Path path) {
        this.dataLoader = new DataLoader(path);
        this.entityValidator = new EntityValidator(path);
    }

    public void validateConsistency() {
        LinkedList<String> lines = new LinkedList<>();
        logger.info("VALIDATION REPORT");
        dataLoader.getLineStreamByClassName(WorkflowInstance.class)
                .skip(1)
                .forEach(line -> {
                    if (line.equals("end")) {
                        try {
                            WorkflowInstance workflowInstance = dataMapper.readEntity(lines, WorkflowInstance.class);
                            entityValidator.validate(workflowInstance);
                        } catch (InstanceMalformedException e) {
                            e.printStackTrace();
                        }
                    } else {
                        lines.add(line);
                    }
                });
    }

    public void generateInstanceReport() {
        LinkedList<String> lines = new LinkedList<>();
        logger.info("REPORT GENERATION");
        dataLoader.getLineStreamByClassName(Workflow.class)
                .skip(1)
                .forEach(line -> {
                    if (line.equals("end")) {
                        Workflow workflow = dataMapper.readEntity(lines, Workflow.class);
                        try {
                            List<WorkflowInstance> list = getInstancesList(workflow);
                            logger.info("Workflow :{} has instance list:{}\n", workflow, list);
                        } catch (WorkflowMalformedException e) {
                            e.printStackTrace();
                        }
                    } else {
                        lines.add(line);
                    }

                });
    }

    public void generateRunningInstancesReport() {
        LinkedList<String> lines = new LinkedList<>();
        dataLoader.getLineStreamByClassName(Workflow.class)
                .skip(1)
                .forEach(line -> {
                    if (line.equals("end")) {
                        try {
                            Workflow workflow = dataMapper.readEntity(lines, Workflow.class);
                            entityValidator.validate(workflow);

                            List<WorkflowInstance> workflowInstanceList = dataLoader
                                    .getInstancesById(workflow.getId());

                            List<WorkflowInstance> runningInstances = getRunningInstances(workflowInstanceList);
                            if (!runningInstances.isEmpty()) {
                                logger.info("{} has {} running instances", workflow, runningInstances.size());

                                runningInstances.forEach(runningInstance -> {
                                    List<Person> personList = dataLoader.getContractor(runningInstance.getAssignee());
                                    personList.forEach(person -> {
                                        logger.info("Contractor is {}", person.getContractorName());
                                    });
                                });
                            }
                        } catch (InstanceMalformedException e) {
                            e.printStackTrace();
                        } catch (WorkflowMalformedException e) {
                            e.printStackTrace();
                        }
                    } else {
                        lines.add(line);
                    }

                });

    }

    private List<WorkflowInstance> getInstancesList(Workflow workflow) throws WorkflowMalformedException {
        entityValidator.validate(workflow);
        return dataLoader
                .getInstancesById(workflow.getId());
    }

    private List<WorkflowInstance> getRunningInstances(List<WorkflowInstance> workflowInstanceList) throws InstanceMalformedException {
        entityValidator.validate(workflowInstanceList);
        return workflowInstanceList.stream()
                .filter(workflowInstance -> workflowInstance.getStatus()
                        .equals(Status.RUNNING)).collect(Collectors.toList());
    }
}
