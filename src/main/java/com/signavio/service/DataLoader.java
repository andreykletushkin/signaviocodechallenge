package com.signavio.service;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import com.signavio.model.Person;
import com.signavio.model.Workflow;
import com.signavio.model.WorkflowInstance;
import com.signavio.parser.DataMapper;
import com.signavio.utils.Matches;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

public class DataLoader {

    private Path directory;
    private final DataMapper dataMapper = new DataMapper();
    private final Map<String, Class<?>> stringToEntityMap = new HashMap<>();
    private final Multimap<Class<?>, Path> classToPathMap;

    {
        stringToEntityMap.put("WORKFLOW INSTANCES", WorkflowInstance.class);
        stringToEntityMap.put("EMPLOYEES", Person.class);
        stringToEntityMap.put("CONTRACTORS", Person.class);
        stringToEntityMap.put("WORKFLOWS", Workflow.class);
    }

    public DataLoader(Path directory) {
        this.directory = directory;
        classToPathMap = generateStringToEntityMap();
    }

    public List<Person> getContractor(String email) {
        LinkedList<String> lines = new LinkedList<>();
        LinkedList<Person> result = new LinkedList<>();
        for (Path path : this.classToPathMap.get(Person.class)) {
            getLineStream(path)
                    .skip(1)
                    .forEach(line -> {
                        if (line.equals("end")) {
                            Person person = dataMapper.readEntity(lines, Person.class);
                            if (Matches.contractorMatch(email, person)) result.add(person);
                        } else {
                            lines.add(line);
                        }
                    });
        }
        return result;
    }

    public Boolean isPersonExists(String email) {
        String result = "";
        for (Path path : this.classToPathMap.get(Person.class)) {
            result = getLineStream(path)
                    .filter(line -> line.split(":").length == 2)
                    .filter(line -> line.split(":")[1].trim().equals(email))
                    .findAny().orElse(null);
            if (result != null) return true;
        }
        return false;
    }

    public boolean isWorkflowExists(Long id) {
        Path path = this.classToPathMap.get(Workflow.class).iterator().next();
         String result = getLineStream(path)
                .filter(line -> line.split(":").length == 2)
                .filter(line -> line.split(":")[1].trim().equals(String.valueOf(id)))
                .findAny().orElse(null);
        if (result != null) return true;
        return false;
    }

    public List getInstancesById(Long parentValue) {
        LinkedList<String> lines = new LinkedList<>();
        LinkedList<WorkflowInstance> result = new LinkedList<>();
        getLineStreamByClassName(WorkflowInstance.class)
                .skip(1)
                .forEach(line -> {
                    if (line.equals("end")) {
                        WorkflowInstance workflowInstance = dataMapper.readEntity(lines, WorkflowInstance.class);
                        if (workflowInstance.getWorkflowId() != null
                                && Matches.contractorMatch(parentValue, workflowInstance))
                            result.add(workflowInstance);

                    } else {
                        lines.add(line);
                    }
                });
        return result;
    }

    public Stream<String> getLineStreamByClassName(Class aClass) {
        try {
            return Files.lines(classToPathMap.get(aClass).stream().findFirst().get());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    private Multimap<Class<?>, Path> generateStringToEntityMap() {
        Multimap<Class<?>, Path> map = ArrayListMultimap.create();
        getFileStream()
                .forEach(fileInDirectory -> {
                    String line = getLineStream(fileInDirectory)
                            .findFirst().orElse(null);
                    map.put(this.stringToEntityMap.get(line), fileInDirectory);
                });
        return map;
    }

    private Stream<Path> getFileStream() {
        try {
            return Files.list(directory);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    private Stream<String> getLineStream(Path file) {
        try {
            return Files.lines(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }


}
